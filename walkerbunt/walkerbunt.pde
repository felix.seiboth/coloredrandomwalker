// RANDOM WALKERS / AGENTS / BROWNIAN MOTION

float stepLength = 10;
int angle = 0;
int stepNum = 10;

// position of our agent
int x;
int y;

// previous position of our agent (previous frame)
int pX;  
int pY;  

void setup(){
  size(800,800);
  background(255);
  
  // initialize the position of the walker
  x = width/2;
  y = width/2;
  pX = x;
  pY = y;
  

}

void draw(){
 //background(255);
  
  // let the agent move, by adding random 
  // values to the position of our walker
  x+=random(-stepLength, stepLength);
  y+=random(-stepLength, stepLength);  
  
  // draw a line between the current and the previous position 
 // line(x, y, pX, pY);
 

  
  angle += 5;
    float variabel = cos(radians(angle)) * 20.0;
    for (int a = 0; a < 360; a += 75) {
      float xbluete = cos(radians(a)) * variabel;
      float ybluete = sin(radians(a)) * variabel;
      fill(random(255), random(255), random(255));
      ellipse(x + xbluete, y + ybluete, variabel, variabel);
  
  fill(random(255), random(255), random(255));
  ellipse(x, y, 10, 10);

    }
    
    
    
    
    
  
  
  // what happens if our agent moves outside of the canvas?
  // let it reappear on the opposite side
  if(x<0) x = width;
  if(x>width) x = 0;
  if(y<0) y = height;
  if(y>height) y = 0;
  
  // store the current position to make it available
  // as previous position in the next frame
  pX = x;
  pY = y;
}
